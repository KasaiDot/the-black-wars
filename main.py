from pyware import *
import sys,random,math
pygame.init()
gamedir='./'

class Character(Sprite):
  def __init__(self,name,imagefile,faction,unit,pos=[500,350]):
    Sprite.__init__(self,window,imagefile,pos)
    self.name=name
    stats=File(gamedir+'stats/'+faction+'/'+unit)
    self.life=self.maxlife=stats.read('life')
    self.defence=stats.read('defence')
    self.mana=self.maxmana=stats.read('mana')
    self.strength=stats.read('strength')
    self.magic=stats.read('magic')
    self.evasion=stats.read('evasion')
    self.accuracy=stats.read('accuracy')
    self.speed=stats.read('speed')
    self.critical=stats.read('critical')
    self.abilities=stats.read('abilities')
    self.AI_labels=stats.read('AI_labels')
    self.effects=[]
    self.attacked=0 # these two are thigs that Battle.round() keeps track of and resets at the end.
    self.dodged=0   #
  def __bool__(self):
    if self.life>0:return True
    else:return False
  def overflow_check(self):
    if self.life>self.maxlife:self.life=int(str(self.maxlife)[:])
    if self.mana>self.maxmana:self.mana=int(str(self.maxmana)[:])
  def dd(self):
    value=self.defence
    for effect in self.effects:
      if effect.stat=='defence':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def str(self):
    value=self.strength
    for effect in self.effects:
      if effect.stat=='strength':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def mgk(self,type):
    value=self.magic[type]
    for effect in self.effects:
      if effect.stat==type+' magic':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def spd(self,type):
    value=self.speed[type]
    for effect in self.effects:
      if effect.stat==type+' speed':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def eva(self):
    value=self.evasion
    for effect in self.effects:
      if effect.stat=='evasion':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def acc(self,type):
    value=self.accuracy[type]
    for effect in self.effects:
      if effect.stat==type+' accuracy':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value
  def crit(self,type):
    value=self.critical[type]
    for effect in self.effects:
      if effect.stat==type+' critical':
        if type(effect.change)==float:value*=effect.change
        else:value+=effect.change
    return value

  def parse_effects(self):
    return 'none'
  def elapse_effects(self):
    for effect in self.effects:
      if effect.duration!=-1:effect.duration-=1
      if effect.duration==0:del self.effects[self.effects.index(effect)]

class Effect(object):
  def __init__(self,stat,change,duration):
    self.stat=stat
    self.change=change
    self.duration=duration

def ring_unit(unit):
  center=[int(average(unit.left(),unit.right())),int(average(unit.top(),unit.bottom()))]
  if 'blue' in unit.name or unit.name in {}:color=(255,0,0)
  elif 'red' in unit.name or unit.name in {'Kai'}:color=(0,0,255)
  elif 'white' in unit.name or unit.name in {}:color=(0,0,0)
  elif 'green' in unit.name or unit.name in {'serpentus','lily'}:color=(255,255,0)
  elif 'Yellow' in unit.name or unit.name in {'Arkterus','Kori'}:color=(0,255,0)
  elif 'black' in unit.name or unit.name in {}:color=(255,255,255)
  else:color=(127,127,127)
  pygame.draw.circle(window,color,center,32,1)

def retarget(team,targetdead):
  viables=[]
  for unit in team.units():
    if unit.life>0 and unit.attacked<4 and not targetdead or targetdead and unit.life<=0:viables.append(unit)
  if viables:return random.choice(viables)

class Team(object):
  def __init__(self,front,midfront,midback,back):
    self.front=front
    self.midfront=midfront
    self.midback=midback
    self.back=back
  def arrange(self):
    window.fill((0,0,0))
    change_difficulty=Button(window,(51,35),(158,11),'change difficulty ('+difficulty+')',(255,255,255),(0,0,0))
    accept=Button(window,(51,55),(98,11),'accept formation',(255,255,255),(0,0,0))
    for x in range(4):pygame.draw.line(window,(255,255,255),(200-x*50,100),(200-x*50,1000))
    positions={1:(437,),2:(343,531),3:(281,437,593),4:(250,375,500,625),5:(250,343,437,531,625),6:(250,325,400,475,550,625),7:(250,312,375,437,500,562,625),8:(250,303,357,411,464,518,571,625),9:(218,273,328,383,437,492,546,601,656),10:(187,243,298,354,410,465,521,576,632,687),11:(187,237,287,337,387,437,487,537,587,637,687)}[len(self.units())]
    index=0
    for character in self.units():
      character.overflow_check()
      character.put(225-self.row(character)*50,positions[index])
      index+=1
    pygame.display.flip()
    selected=self.units()[0]
    done=False
    while not done:
      clock.tick(10)
      for event in pygame.event.get():
        if event.type==pygame.QUIT:sys.exit()
        if event.type==pygame.MOUSEBUTTONDOWN:
          for character in self.units():
            if character.recttest(event.pos):selected=character
          if accept.test(event.pos):
            accept.down=True
            pygame.display.flip()
        if event.type==pygame.MOUSEBUTTONUP:
          if accept.test(event.pos) and accept.down:done=True
          accept.down=False
          pygame.display.flip()
        if event.type==pygame.KEYDOWN:
          if event.key==pygame.K_LEFT and selected.pos[0]>=75:
            pygame.draw.rect(window,(0,0,0),(selected.left(),selected.top(),selected.size[0]+1,selected.size[1]+1))
            selected.move(-50,0)
            pygame.display.flip()
          if event.key==pygame.K_RIGHT and selected.pos[0]<=125:
            pygame.draw.rect(window,(0,0,0),(selected.left(),selected.top(),selected.size[0]+1,selected.size[1]+1))
            selected.move(50,0)
            pygame.display.flip()
    newfront=[]
    newmidfront=[]
    newmidback=[]
    newback=[]
    for character in self.units():
      if character.pos[0]==175:newfront+=[character]
      if character.pos[0]==125:newmidfront+=[character]
      if character.pos[0]==75:newmidback+=[character]
      if character.pos[0]==25:newback+=[character]
    self.front=newfront
    self.midfront=newmidfront
    self.midback=newmidback
    self.back=newback
  def units(self):return self.front+self.midfront+self.midback+self.back
  def standing_units(self):
    all=self.units()
    standing=[]
    for unit in all:
      if unit:standing.append(unit)
    return standing
  def row(self,member):
    for unit in self.front:
      if unit is member:return 1
    for unit in self.midfront:
      if unit is member:return 2
    for unit in self.midback:
      if unit is member:return 3
    return 4
  def gc(self,row):
    guard=0
    if row=='midfront' or row==2:
      for unit in self.front:guard+=unit.life/20
    elif row=='midback' or row==3:
      for unit in self.front:guard+=unit.life/15
      for unit in self.midfront:guard+=unit.life/20
    elif row=='back' or row==4:
      for unit in self.front:guard+=unit.life/10
      for unit in self.midfront:guard+=unit.life/15
      for unit in self.midback:guard+=unit.life/20
    return int(guard+0.5)

class Fight(object):
  def __init__(self,player_team,enemy_team,AI,environment,mandatory=False):
    self.player_team=player_team
    self.player_team.arrange()
    self.enemy_team=enemy_team
    self.AI=AI
    self.mandatory=mandatory
    self.environment=environment
    self.background=self.draw(environment,resetbg=True)
  def draw(self,environment,resetbg=False):
    if resetbg:
      window.fill((127,127,127))
      background=load_area(environment,mode='battle')
    else:
      if environment:environment.draw()
    pygame.draw.line(window,(255,255,255),(0,100),(1000,100))
    index=0
    positions={1:(437,),2:(343,531),3:(281,437,593),4:(250,375,500,625),5:(250,343,437,531,625),6:(250,325,400,475,550,625),7:(250,312,375,437,500,562,625),8:(250,303,357,411,464,518,571,625),9:(218,273,328,383,437,492,546,601,656),10:(187,243,298,354,410,465,521,576,632,687),11:(187,237,287,337,387,437,487,537,587,637,687)}[len(self.player_team.units())]
    for unit in self.player_team.units():
      unit.put(200-self.player_team.row(unit)*25,positions[index])
      if unit.life<=0:self.ring_unit(unit)
      index+=1
    index=0
    positions={1:(437,),2:(343,531),3:(281,437,593),4:(250,375,500,625),5:(250,343,437,531,625),6:(250,325,400,475,550,625),7:(250,312,375,437,500,562,625),8:(250,303,357,411,464,518,571,625),9:(218,273,328,383,437,492,546,601,656),10:(187,243,298,354,410,465,521,576,632,687),11:(187,237,287,337,387,437,487,537,587,637,687)}[len(self.enemy_team.units())]
    for unit in self.enemy_team.units():
      unit.put(800+self.enemy_team.row(unit)*25,positions[index])
      if unit.life<=0:ring_unit(unit)
      index+=1
    pygame.display.flip()
    if resetbg:return background
  def attack(self,attacker,target,acc,reduction,type,mag,fluct_lbound,fluct_ubound):
    target.attacked+=1
    if attacker in self.player_team.units():
      score,pps=buttons(attacker.acc(type)*acc,target.eva()/reduction)
      result=is_hit('attack',score)
    else:
      score,pps=buttons(target.eva()/reduction,attacker.acc(type)*acc)
      result=is_hit('defence',score)
    critical=False
    if result[0]=='miss':
      description=attacker.name+' misses. details:'
      description+='\nbuttons hit:      '+str(score)+'/8'
      description+='\nability accuracy: '+str(acc)
    elif result[0]=='hit':
      fluctuation=random.randint(fluct_lbound,fluct_ubound)/100
      if type=='magic':stat=attacker.mgk('offensive')
      else:stat=attacker.str()
      damage=mag*stat*result[1]*fluctuation
      if fluctuation>=1:fluctuation='+'+str(round(fluctuation-1,2))[2:]+'%'
      else:fluctuation='-'+str(round(fluctuation-1,2))[3:]+'%'
      if random.randint(1,100)<=attacker.crit(type):
        critical=True
        damage*=1.5
      damage-=target.dd()
      if attacker in self.player_team.units():gc=self.enemy_team.gc(self.enemy_team.row(target))
      else:gc=self.player_team.gc(self.player_team.row(target))
      damage-=gc
      damage=int(damage)
      target.life-=damage
      incapacitated=''
      if target.life<0:
        ring_unit(target)
        incapacitated=", incapacitating them"
      description=attacker.name+' deals '+str(damage)+' damage to '+target.name+incapacitated+'. calculation:'
      description+='\ncharacter stat:      '+str(stat)
      description+='\nability multiplier:  '+str(mag)
      description+='\ngrade modifier:      '+str(round(result[1],2))
      description+='\nrandom variance:     '+fluctuation
      description+='\ncritical:            '+{True:'yes',False:'no'}[critical]
      description+='\ntarget defence:      '+str(target.dd())
      description+='\nrow defence:         '+str(gc)
      description+='\nbuttons hit:         '+str(score)+'/8'
      description+='\nability accuracy:    '+str(acc)
    elif result[0]=='counterattack':
      fluctuation=random.randint(85,115)/100
      damage=3/4*target.str()*result[1]
      if fluctuation>=1:fluctuation='+'+str(round(fluctuation-1,2))[2:]+'%'
      else:fluctuation='-'+str(round(fluctuation-1,2))[3:]+'%'
      if random.randint(1,100)<=target.crit(type):
        critical=True
        damage*=1.5
      damage-=attacker.dd()
      if attacker in self.player_team.units():gc=self.player_team.gc(self.player_team.row(attacker))
      else:gc=self.enemy_team.gc(self.enemy_team.row(attacker))
      damage-=gc
      damage=int(damage)
      attacker.life-=damage
      incapacitated=''
      if attacker.life<0:
        ring_unit(attacker)
        incapacitated=", incapacitating them"
      description=target.name+' counterattacks and deals '+str(damage)+' damage to '+attacker.name+incapacitated+'. calculation:'
      description+='\ncharacter strength:       '+str(target.str())
      description+='\ncounterattack multiplier: .75'
      description+='\ngrade modifier:           '+str(round(result[1],2))
      description+='\nrandom variance:          '+fluctuation
      description+='\ncritical:                 '+{True:'yes',False:'no'}[critical]
      description+='\nattacker defence:         '+str(attacker.dd())
      description+='\nrow defence:              '+str(gc)
      description+='\nbuttons hit:              '+str(score)+'/8'
      description+='\nability accuracy:         '+str(acc)
    self.message(description)
    return result[0]
  def message(self,text):
    window.fill((0,0,0),(0,0,1000,100))
    write(window,[1,1],text,(255,255,255),1000)
    pygame.display.flip()
    wait([pygame.K_SPACE],clock)
    window.fill((0,0,0),(0,0,1000,100))
    pygame.display.flip()
  def disp_actions(self,actions):
    y=-9
    for character in self.player_team.units():
      y+=10
      write(window,(1,y),character.name+':',(255,255,255))
      if not character:
        write(window,(100,y),'<incapacitated>',(255,255,255))
        continue
      action=actions[character]
      if not action:
        write(window,(100,y),'<no action queued>',(255,255,255))
        continue
      write(window,(100,y),action[2],(255,255,255))
      write(window,(300,y),'->',(255,255,255))
      write(window,(400,y),action[3].name,(255,255,255))
      write(window,(600,y),'(time: '+{True:'+',False:''}[action[0]>0]+str(action[0])+')',(255,255,255))
  def round(self):
    displayed=None
    schedule={}
    # it's the player's turn first
    proceed=False
    window.fill((127,127,127),(0,0,1000,100))
    done=Button(window,(2,85),(27,11),'done',(255,255,255),(0,0,0))
    done.draw()
    actions={}
    for character in self.player_team.units():actions[character]=None
    self.disp_actions(actions)
    pygame.display.flip()
    while not proceed: # while the player's turn is not over
      clock.tick(20)
      for event in pygame.event.get():
        if event.type==pygame.QUIT:pygame.quit();sys.exit()
        if event.type==pygame.MOUSEBUTTONDOWN:
          if done.test(event.pos):
            done.down=True
            pygame.display.flip()
          for character in self.player_team.units():
            if character.recttest(event.pos) and character.life>0: # if a standing character has been clicked on
              window.fill((127,127,127),(0,0,1000,100))
              action=None;category=None;targets=None;cancelled=False
              while not action:
                if not category:category=get_action_type(character.name,character.abilities)
                if category=='back':cancelled=True;break
                action=get_action(character.name,category,character.abilities,character.mana)
                if action==NotImplemented:category=None;action=None # if player hit the back button, reset and prompt again for a category
              if cancelled:break
              if action in NON_TARGETED:target=None
              elif action in SINGLE_TARGETED:
                target=None
                pygame.draw.rect(window,(127,127,127),(10,50,100,7),0)
                write(window,(10,50),'select a target',(255,255,255))
                pygame.display.flip()
                while not target:
                  clock.tick(20)
                  for event in pygame.event.get():
                    if event.type==pygame.QUIT:pygame.quit();sys.exit()
                    if event.type==pygame.MOUSEBUTTONDOWN:
                      for unit in self.player_team.units()+self.enemy_team.units():
                        if unit.recttest(event.pos):target=unit
              pygame.draw.rect(window,(127,127,127),(10,50,100,7),0) # once a target is selected, the "select target" message needs to go away.
              pygame.display.flip()
              time=ABILITY_SPEEDS[action]
              if not action in NON_TARGETED:time+=character.spd(ABILITY_TYPES[action])
              actions[character]=[time,character,action,target] # since it's a dictionary, this will automatically override any previously selected action thus preventing the same character from doing multiple things per turn.
              done.draw() # erased this earlier, so it needs to be put back
              pygame.display.flip()
              break
          self.disp_actions(actions)
          pygame.display.flip()
        if event.type==pygame.MOUSEBUTTONUP:
          if done.test(event.pos) and done.down:proceed=True
          done.down=False
          pygame.display.flip()
        if event.type==pygame.MOUSEMOTION:
          found=False
          for unit in self.player_team.units()+self.enemy_team.units():
            if unit.recttest(event.pos):
              found=True
              if displayed!=unit.name:
                window.fill((127,127,127),(0,0,1000,100))
                displayed=unit.name
                if unit in self.player_team.units():draw_stats(unit,self.player_team.row(unit))
                else:draw_stats(unit,self.enemy_team.row(unit))
              break
          if not found and displayed:
            window.fill((127,127,127),(0,0,1000,100))
            displayed=None
            self.disp_actions(actions)
            done.draw()
            pygame.display.flip()
    for character in actions:
      action=actions[character] # making a shortcut variable to avoid lots of indexing
      if not action:continue
      if action[0] not in schedule:schedule[action[0]]=[[action[1],action[2],action[3]]] # if there are not yet any other events at that time mark, then the dictionary will not have the key and it will need to be added. I used double brackets so the entire event would be stored as one (list) element.
      else:schedule[action[0]].append([action[1],action[2],action[3]]) # otherwise, the key is already there and we can just append to its value.
    # now it's the enemy's turn
    window.fill((127,127,127),(0,0,1000,100))
    write(window,(400,47),"enemies are thinking...",(255,255,255))
    actions=AI_calc(self.enemy_team,self.AI,self.player_team)
    for action in actions:
      a=actions[action]
      if a[0] not in schedule:schedule[a[0]]=[[a[1],a[2],a[3]]]
      else:schedule[a[0]].append([a[1],a[2],a[3]])
      window.fill((0,0,0),(0,0,1000,100)) # reset display
      pygame.display.flip()

    while schedule:
      next=-100
      for time in schedule:
        if time>next:next=int(str(time)[:])
      event_list=randomize(schedule[next])
      for event in event_list:
        acter=event[0]
        ability=event[1]
        window.fill((0,0,0),(0,0,1000,100))
        if acter.life<=0:
          self.message(acter.name+' is incapacitated and cannot do '+ability)
          continue
        target_str=event[2].name
        if event[2].__class__==Character:
          if event[2].life<=0 or event[2].attacked==4:
            target_str+=' (retargeted)'
            if acter in self.player_team.units() and ability not in ALLY_TARGETED or acter in self.enemy_team.units() and ability in ALLY_TARGETED:event[2]=retarget(self.enemy_team,ability in DEAD_TARGETED)
            else:event[2]=retarget(self.player_team,ability in DEAD_TARGETED)
            if not event[2]:self.message(acter.name+' cannot do '+ability+' because there are no valid targets');continue
        elif event[2].__class__==list:
          index=1
          for target in event[2]:
            if len(event[2])==index:target_str+=target.name
            elif len(event[2])==2 and index==1:target_str+=target.name+' and '
            elif index==len(event[2])-1:target_str+=target.name+', and '
            else:target_str+=target.name+', '
            index+=1
        window.fill((0,0,0),(0,0,1000,100))
        if target_str:self.message('next event: '+acter.name+' doing '+ability+' on '+target_str)
        else:self.message('next event: '+acter.name+' doing '+ability)

        acter.mana-=ABILITY_COSTS[ability]
        if ability in MISC_ABILITIES:
          if ability=='guard':
            increment=int(actor.maxlife/10+0.49)
            acter.effects.append(Effect('defence',increment,1))
            # boost gc
          if ability=='defend':
            increment=int(actor.maxlife/20+0.49)
            acter.effects.append(Effect('defence',increment,1))
            acter.effects.append(Effect('evasion',1.25,1))
            # boost gc
          if ability=='flee':
            pass
        elif ability in SINGLE_TARGETED:
          target=event[2]
          reduction=math.pow(1.3,min(target.dodged,3))
          if ability=='attack':
            result=self.attack(acter,target,1,reduction,'melee',1,85,115)
          if ability=='stab':
            result=self.attack(acter,target,0.85,reduction,'melee',1.3,85,115)
          if ability=='speed slash':
            result=self.attack(acter,target,1.25,reduction,'melee',0.8,85,115)
          if ability=='electric strike':
            result=self.attack(acter,target,1.05,reduction,'melee',1.3,85,115)
          if ability=='flaming smash':
            self.message('attacker swings their weapon...')
            result=self.attack(acter,target,0.75,reduction,'melee',1.4,85,115)
            self.message('it emits a wave of fire...')
            result=self.attack(acter,target,2,reduction,'melee',0.6,85,115)
          if ability=='shoot':
            result=self.attack(acter,target,1,reduction,'ranged',1,85,115)
          if ability=='power shot':
            result=self.attack(acter,target,1.1,reduction,'ranged',1.2,85,115)
          if ability=='headshot':
            result=self.attack(acter,target,0.65,reduction,'ranged',1.4,85,115)
          if ability=='quickshot':
            result=self.attack(acter,target,0.65,reduction,'ranged',0.8,85,115)
          if ability in {'fireball','ice shard','light blast','rock','zap','shadow'}:
            result=self.attack(acter,target,1,reduction,'magic',1,85,115)
          if ability=='heal':pass
            #
          if 'result' in dir() and result!='hit':target.dodged+=1
      del schedule[next] # once all the events at this time have occurred, they must be deleted so they don't keep happening indefinitely.

    for unit in self.player_team.units()+self.enemy_team.units():
      unit.mana+=int((unit.maxmana+random.randint(1,99))/100)
      unit.elapse_effects()
      unit.overflow_check()
      unit.attacked=0
      unit.dodged=0

    window.fill((127,127,127),(0,0,1000,100)) # re-grey the panel for the start of the next round

  def over(self):
    if self.player_team.standing_units():
      if self.enemy_team.standing_units():return None
      return 'victory'
    if self.enemy_team.standing_units():return 'defeat'
    return 'tie'


def AI_calc(enemy_team,level,player_team):
  actions={}
  if level=='stupid':
    for unit in enemy_team.standing_units():
      if 'melee' in unit.AI_labels:
        abilities=set(unit.abilities).intersection(MELEE_ABILITIES)
        options=abilities.copy() # this is to prevent a "set changed size suring iteration" error
        for ability in abilities:
          if unit.mana<ABILITY_COSTS[ability]:options.remove(ability)
        ability=random.choice(list(options))
        actions[unit]=[unit.spd('melee')+ABILITY_SPEEDS[ability],unit,ability,random.choice(player_team.standing_units())]

  if level=='dumb':
    for unit in enemy_team.standing_units():
      if 'melee' in unit.AI_labels:
        abilities=set(unit.abilities).intersection(MELEE_ABILITIES)
        options=abilities.copy()
        for ability in abilities:
          if unit.mana<ABILITY_COSTS[ability]:options.remove(ability)
        target=None
        old_speed_ratio=0
        old_damage_ratio=0
        for character in player_team.standing_units():
          speed_ratio=unit.acc('melee')/character.eva()
          damage_ratio=(unit.str()-character.dd()-player_team.gc(player_team.row(character)))/unit.str()
          if speed_ratio*damage_ratio*random.randint(67,150)/100>old_speed_ratio*old_damage_ratio:target=character
          old_speed_ratio=speed_ratio
          old_damage_ratio=damage_ratio
        if speed_ratio<damage_ratio:
          if speed_ratio<0.75:
            accurates=options.intersection(ACCURATE_ABILITIES)
            if accurates:options=accurates
            else:
              accurates=options.intersection(NONACCURATE_ABILITIES)
              if accurates:options=accurates
          if damage_ratio<0.75:
            damaging=options.intersection(DAMAGING_ABILITIES)
            if damaging:options=damaging
            else:
              damaging=options.intersection(NONACCURATE_ABILITIES)
              if damaging:options=damaging
        else:
          if damage_ratio<0.75:
            damaging=options.intersection(DAMAGING_ABILITIES)
            if damaging:options=damaging
            else:
              damaging=options.intersection(NONDAMAGING_ABILITIES)
              if damaging:options=damaging
          if speed_ratio<0.75:
            damaging=options.intersection(DAMAGING_ABILITIES)
            if damaging:options=damaging
            else:
              damaging=options.intersection(NONDAMAGING_ABILITIES)
              if damaging:options=damaging
        ability=random.choice(list(options))
        actions[unit]=[unit.spd('melee')+ABILITY_SPEEDS[ability],unit,ability,target]

  return actions

def draw_stats(unit,row):
  write(window,[5,5],unit.name,(255,255,255))
  write(window,[25,20],str(unit.life)+'/'+str(unit.maxlife),(255,255,255))
  Bar(window,[100,20],[400,20],[255,0,0],(0,0,0),(255,255,255),unit.maxlife,unit.life)
  write(window,[25,60],str(unit.mana)+'/'+str(unit.maxmana),(255,255,255))
  Bar(window,[100,60],[400,20],[0,0,255],(0,0,0),(255,255,255),unit.maxmana,unit.mana)
  if unit.str()!=unit.strength:string=str(unit.str())+'('+str(unit.strength)+')'
  else:string=str(unit.strength)
  write(window,[550,10],string+' strength',(255,255,255))
  if unit.dd()!=unit.defence:string=str(unit.dd())+'('+str(unit.defence)+')'
  else:string=str(unit.defence)
  write(window,[550,30],string+' defence',(255,255,255))
  if unit.mgk('offensive')!=unit.magic['offensive']:string=str(unit.mgk('offensive'))+'('+str(unit.magic['offensive'])+')'
  else:string=str(unit.magic['offensive'])
  write(window,[550,50],string+' offensive magic',(255,255,255))
  if unit.mgk('defensive')!=unit.magic['defensive']:string=str(unit.mgk('defensive'))+'('+str(unit.magic['defensive'])+')'
  else:string=str(unit.magic['defensive'])
  write(window,[550,60],string+' defensive magic',(255,255,255))
  if unit.mgk('passive')!=unit.magic['passive']:string=str(unit.mgk('passive'))+'('+str(unit.magic['passive'])+')'
  else:string=str(unit.magic['passive'])
  write(window,[550,70],string+' passive magic',(255,255,255))
  if unit.acc('melee')!=unit.accuracy['melee']:string=str(unit.acc('melee'))+'('+str(unit.accuracy['melee'])+')'
  else:string=str(unit.accuracy['melee'])
  write(window,[690,10],string+' melee accuracy',(255,255,255))
  if unit.acc('ranged')!=unit.accuracy['ranged']:string=str(unit.acc('ranged'))+'('+str(unit.accuracy['ranged'])+')'
  else:string=str(unit.accuracy['ranged'])
  write(window,[690,20],string+' ranged accuracy',(255,255,255))
  if unit.acc('magic')!=unit.accuracy['magic']:string=str(unit.acc('magic'))+'('+str(unit.accuracy['magic'])+')'
  else:string=str(unit.accuracy['magic'])
  write(window,[690,30],string+' magic accuracy',(255,255,255))
  if unit.eva()!=unit.evasion:string=str(unit.eva())+'('+str(unit.evasion)+')'
  else:string=str(unit.evasion)
  write(window,[690,50],string+' evasion',(255,255,255))
  write(window,[690,70],{1:'frontmost',2:'midfront',3:'midback',4:'backmost'}[row]+' row',(255,255,255))
  if unit.spd('melee')!=unit.speed['melee']:string=str(unit.spd('melee'))+'('+str(unit.speed['melee'])+')'
  else:string=str(unit.speed['melee'])
  write(window,[830,10],string+' melee time modifier',(255,255,255))
  if unit.spd('ranged')!=unit.speed['ranged']:string=str(unit.spd('ranged'))+'('+str(unit.speed['ranged'])+')'
  else:string=str(unit.speed['ranged'])
  write(window,[830,20],string+' ranged time modifier',(255,255,255))
  if unit.spd('magic')!=unit.speed['magic']:string=str(unit.spd('magic'))+'('+str(unit.speed['magic'])+')'
  else:string=str(unit.speed['magic'])
  write(window,[830,30],string+' magic time modifier',(255,255,255))
  if unit.crit('melee')!=unit.critical['melee']:string=str(unit.crit('melee'))+'('+str(unit.critical['melee'])+')'
  else:string=str(unit.critical['melee'])+'%'
  write(window,[830,50],string+' melee critical chance',(255,255,255))
  if unit.crit('ranged')!=unit.critical['ranged']:string=str(unit.crit('ranged'))+'('+str(unit.critical['ranged'])+')'
  else:string=str(unit.critical['ranged'])+'%'
  write(window,[830,60],string+' ranged critical chance',(255,255,255))
  if unit.crit('magic')!=unit.critical['magic']:string=str(unit.crit('magic'))+'('+str(unit.critical['magic'])+')'
  else:string=str(unit.critical['magic'])+'%'
  write(window,[830,70],string+' magic critical chance',(255,255,255))
  pygame.display.flip()

def get_action_type(name,abilities):
  selected=False
  write(window,[25,25],name+"'s turn",(255,255,255))
  write(window,[25,70],'choose an action',(255,255,255))
  buttons=[Button(window,[160,9],[90,90],melee_img,(0,0,0),(255,255,255),True),Button(window,[300,9],[90,90],ranged_img,(0,0,0),(255,255,255),True),Button(window,[440,9],[90,90],off_mgk_img,(0,0,0),(255,255,255),True),Button(window,[580,9],[90,90],def_mgk_img,(0,0,0),(255,255,255),True),Button(window,[720,9],[90,90],psv_mgk_img,(0,0,0),(255,255,255),True),Button(window,[860,9],[90,90],misc_img,(0,0,0),(255,255,255),True),Button(window,[2,85],[27,11],'back',(255,255,255),(0,0,0))]
  for button in buttons:button.draw()
  write(window,[205,1],str(len(set(abilities).intersection(MELEE_ABILITIES))),(255,255,255))
  write(window,[345,1],str(len(set(abilities).intersection(RANGED_ABILITIES))),(255,255,255))
  write(window,[485,1],str(len(set(abilities).intersection(OFF_MAGIC_ABILITIES))),(255,255,255))
  write(window,[625,1],str(len(set(abilities).intersection(DEF_MAGIC_ABILITIES))),(255,255,255))
  write(window,[765,1],str(len(set(abilities).intersection(PSV_MAGIC_ABILITIES))),(255,255,255))
  write(window,[905,1],str(len(set(abilities).intersection(MISC_ABILITIES))),(255,255,255))
  pygame.display.flip()
  while not selected:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEBUTTONDOWN:
        for button in buttons:
          if button.test(event.pos):
            button.down=True
            pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP:
        for button in buttons:
          if button.test(event.pos) and button.down:
            selected={buttons[0]:'melee',buttons[1]:'ranged',buttons[2]:'offensive magic',buttons[3]:'defensive magic',buttons[4]:'passive magic',buttons[5]:'miscellaneous',buttons[6]:'back'}[button]
            break
          button.down=False
          pygame.display.flip()
  window.fill((127,127,127),(0,0,1000,100))
  pygame.display.flip()
  return selected

def get_action(name,category,abilities,mana):
  index=0
  visible_abilities=[]
  for ability in abilities:
    if category=='melee' and ability in MELEE_ABILITIES:visible_abilities.append(ability)
    elif category=='ranged' and ability in RANGED_ABILITIES:visible_abilities.append(ability)
    elif category=='offensive magic' and ability in OFF_MAGIC_ABILITIES:visible_abilities.append(ability)
    elif category=='defensive magic' and ability in DEF_MAGIC_ABILITIES:visible_abilities.append(ability)
    elif category=='passive magic' and ability in PSV_MAGIC_ABILITIES:visible_abilities.append(ability)
    elif category=='miscellaneous' and ability in MISC_ABILITIES:visible_abilities.append(ability)
    else:index+=1
  selected=False;displayed=None
  write(window,[25,25],name+"'s turn",(255,255,255))
  write(window,[25,70],'choose an action',(255,255,255))
  pygame.draw.line(window,(255,255,255),(0,80),(1000,80))
  actions={Button(window,[120,45],[40,9],'back',(255,255,255),(0,0,0)):NotImplemented}
  x=180
  for ability in visible_abilities:
    if mana<ABILITY_COSTS[ability]:color=[191,191,191]
    else:color=(255,255,255)
    actions[Button(window,[x,10],[120,10],ability,color,(0,0,0))]=ability
    x+=160
    if x>870:x=180;y+=20
  pygame.display.flip()
  while not selected:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEMOTION:
        found=False
        for button in actions:
          if button.test(event.pos):
            found=True
            if displayed!=button:
              displayed=button
              write(window,(1,81),ABILITY_DESCRIPTIONS[actions[button]],(255,255,255),1000)
              pygame.display.flip()
        if not found and displayed:
          displayed=False
          pygame.draw.rect(window,(127,127,127),(1,81,999,18),0)
          pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONDOWN:
        for button in actions:
          if button.test(event.pos) and button.color==(255,255,255):
            button.down=True
            pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP:
        for button in actions:
          if button.test(event.pos) and button.down:
            selected=actions[button]
          button.down=False
          pygame.display.flip()
  window.fill((127,127,127),(0,0,1000,100))
  pygame.display.flip()
  return selected

def buttons(player_stat,enemy_stat):
  length=timebar_lengths[difficulty]
  pygame.draw.rect(window,(0,0,0),(0,0,1000,100),0)
  hits=0
  pps_bounds=[random.randint(400,1400),random.randint(1400,2400)]
  spd_factor=player_stat/enemy_stat
  pps_bounds[0]=int(pps_bounds[0]/spd_factor)
  pps_bounds[1]=int(pps_bounds[1]/spd_factor)
  for x in range(8):
    ppcs=random.randint(pps_bounds[0],pps_bounds[1])/100
    pygame.draw.rect(window,(0,0,0),(925,25,51,51),0)
    timebar=Bar(window,(475-int(length/2),25),(length,50),(255,255,255),(0,0,0),(255,255,255),length,0)
    button=random.choice(('up','down','left','right'))
    if button=='up':
      pygame.draw.line(window,(255,255,255),(950,25),(950,75))
      pygame.draw.line(window,(255,255,255),(925,50),(950,25))
      pygame.draw.line(window,(255,255,255),(975,50),(950,25))
    if button=='down':
      pygame.draw.line(window,(255,255,255),(950,25),(950,75))
      pygame.draw.line(window,(255,255,255),(925,50),(950,75))
      pygame.draw.line(window,(255,255,255),(975,50),(950,75))
    if button=='left':
      pygame.draw.line(window,(255,255,255),(925,50),(975,50))
      pygame.draw.line(window,(255,255,255),(925,50),(950,25))
      pygame.draw.line(window,(255,255,255),(925,50),(950,75))
    if button=='right':
      pygame.draw.line(window,(255,255,255),(925,50),(975,50))
      pygame.draw.line(window,(255,255,255),(975,50),(950,25))
      pygame.draw.line(window,(255,255,255),(975,50),(950,75))
    while timebar.fill<length:
      pygame.display.flip()
      clock.tick(100)
      for event in pygame.event.get():
        if event.type==pygame.QUIT:pygame.quit();sys.exit()
        if event.type==pygame.KEYDOWN and timebar.fill>100:
          try:
            if {pygame.K_UP:'up',pygame.K_DOWN:'down',pygame.K_LEFT:'left',pygame.K_RIGHT:'right'}[event.key]==button:
              hits+=1
              pps_bounds=[int(pps_bounds[0]*1.08),int(pps_bounds[1]*1.08)]
            else:pps_bounds=[int(pps_bounds[0]*0.92),int(pps_bounds[1]*0.92)]
            timebar.fill=length
          except KeyError:pass
      timebar.fill+=int((ppcs+random.randint(0,100)/100))
      pygame.draw.line(window,(255,255,255),(475-int(length/2)+100,25),(475-int(length/2)+100,75))
      if timebar.fill>length:timebar.fill=length
  return hits,pps_bounds

def is_hit(mode,score):
  r=random.randint(1,100)
  if mode=='attack':
    if score==0:
      if r<=50:return ['counterattack',3/3]
      return ['counterattack',4/3]
    if score==1:
      if r<=34:return ['counterattack',2/3]
      if r<=67:return ['counterattack',3/3]
      return ['miss']
    if score==2:
      return ['miss']
    if score==3:
      if r<=50:return ['miss']
      return ['hit',2/3]
    if score==4:
      if r<=50:return ['miss']
      if r<=75:return ['hit',2/3]
      return ['hit',3/3]
    if score==5:
      if r<=50:return ['hit',2/3]
      return ['hit',3/3]
    if score==6:
     return ['hit',3/3]
    if score==7:
      if r<=50:return ['hit',3/3]
      return ['hit',4/3]
    if score==8:
      return ['hit',4/3]
  else:
    if score==0:
      return ['hit',4/3]
    if score==1:
      if r>50:return ['hit',3/3]
      return ['hit',4/3]
    if score==2:
      return ['hit',3/3]
    if score==3:
      if r>50:return ['hit',3/3]
      return ['hit',2/3]
    if score==4:
      return ['hit',2/3]
    if score==5:
      if r>50:return ['hit',2/3]
      return ['miss']
    if score==6:
      return ['miss']
    if score==7:
      if r>34:return ['counterattack',2/3]
      if r>67:return ['counterattack',3/3]
      return ['miss']
    if score==8:
      if r>50:return ['counterattack',4/3]
      return ['counterattack',3/3]

def main_menu():
  window.fill((0,0,0))
  write(window,[455,46],'the black wars!',(255,255,255))
  new=Button(window,[465,190],[75,20],'new game',(255,255,255),[0,255,0])
  tut=Button(window,[465,290],[75,20],'tutorial',[0,0,255],[255,0,0])
  load=Button(window,[465,390],[75,20],'load game',[255,255,0],(0,0,0))
#  write(window,[0,600],"note: the black wars is available in novel format, and in the case of discrepancy, the novel overrides the game. to read it, simply open the game directory and open the .html files in the novel directory inside using a browser.",(255,255,255),1000)
  pygame.display.flip()
  music=pygame.mixer.Sound(gamedir+'sound/music/a_chromatic_adventure.ogg')
  music.play(-1)
  while True:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEBUTTONDOWN and event.button==1:
        if new.test(event.pos):new.down=True;pygame.display.flip()
        if tut.test(event.pos):tut.down=True;pygame.display.flip()
        if load.test(event.pos):load.down=True;pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP and event.button==1:
        if new.test(event.pos) and new.down:new_game(get_slot(),music)
        if tut.test(event.pos) and tut.down:
          tutorial()
          write(window,[455,46],'the black wars!',(255,255,255)) # we don't need to redraw the buttons since their .down properties will be changed in just a moment, which will result in them automatically redrawing themselves.
#  write(window,[0,600],"note: the black wars is available in novel format, and in the case of discrepancy, the novel overrides the game. to read it, simply open the game directory and open the .html files in the novel directory inside using a browser.",(255,255,255),1000)
        if load.test(event.pos) and load.down:load_game(get_slot(),music)
        new.down=False;load.down=False;tut.down=False;pygame.display.flip()

def tutorial():
  window.fill((0,0,0))
  write(window,[455,46],'tutorial menu',(255,255,255))
  controls=Button(window,[465,140],[75,20],'the basics',(255,255,255),(0,0,0))
  stats=Button(window,[465,240],[75,20],'stats',(255,255,255),(0,0,0))
  battles=Button(window,[465,340],[75,20],'battles',(255,255,255),(0,0,0))
  back=Button(window,[465,440],[75,20],'back',(255,255,255),(0,0,0))
  pygame.display.flip()
  done=False
  while not done:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEBUTTONDOWN and event.button==1:
        if controls.test(event.pos):controls.down=True;pygame.display.flip()
        if stats.test(event.pos):stats.down=True;pygame.display.flip()
        if battles.test(event.pos):battles.down=True;pygame.display.flip()
        if back.test(event.pos):back.down=True;pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP and event.button==1:
        if controls.test(event.pos) and controls.down:
          window.fill((0,0,0))
          write(window,[1,1],"This game works in a series of scenes, between which it will flow seamlessly (except for loading screens) and autosave after each one. each scene is either a cinematic, field, skirmish, or battle. I will proceed to briefly describe each mode.\n\ncinematic scenes are very simple: all you must do is read the dialog on the top, watch for movement in the scene below, and press enter to continue. field mode is almost as simple: you will be placed in an area and allowed to walk around freely using the arrow keys, with your objective displayed on the top.\n\nskirmish mode is the first one that really needs an explanation. in this mode, your party has encountered hostile forces and must defeat them. before the battle begins you will be given a chance to adjust your party's formation (more on this in the stats tutorial) but this won't matter in the beginning since for the first few fights in the game you only have one person. during your turn, you can mouseover any unit in the battle to see life, mana, and other stats displayed on the top. to queue an action for one of your characters (or change one that is already queued), click them and select the appropriate category button, then the desired action. most actions will prompt you for a target. when you have selected an action for all of your characters, click done.\n\nthe actions that you and the enemy have selected will then unfold in an order determined by various stats and modifiers. for every action that can be dodged, you will be given a test of skill to determine the accuracy. a timebar will appear on the top, filling quickly, and you are to press the arrow displayed to its right as fast as you can. after eight arrows, your number of hits will be used to determine the accuracy of the attack (you want to press the buttons whether you are the attacker or not). it can be a high-grade hit (4/3 damage), a normal hit, a low-grade hit (2/3 damage), a miss, or a high/medium/low-grade counterattack.\n\ninstructions on battle mode coming later.\n\n\npress enter to go back to the help menu.",(255,255,255),1000)
          pygame.display.flip()
          wait([pygame.K_RETURN],clock)
          window.fill((0,0,0))
          write(window,[455,46],'tutorial menu',(255,255,255))
        if stats.test(event.pos) and stats.down:
          window.fill((0,0,0))
          write(window,[1,1],"there are basically eleven stats that each character has:\n\n life - the amount of damage you can take before being incapacitated\n strength - determines the base damage dealt by non-magical attacks\n defence - deducted from the damage of all attacks that hit you\n\n speed - makes your actions happen sooner in the round (this is separated into melee, ranged, and magic speed)\n accuracy - makes the timebar fill slower on your attacks (separated in the same way)\n critical chance - this should be self-explanatory, gives you a chance to inflict massively more damage than normal (also separated)\n evasion - makes the timebar fill slower when you are attacked (not separate)\n\n mana - required by pretty much anything other than normal attacks and fleeing (this regenerates, but very slowly)\n offensive magic - determines the base magnitude of offensive magic\n defensive magic - determines the base magnitude of defensive magic\n passive magic - determines the base magnitude of passive magic, such as buffs and nerfs.\n\nIn full detail, this is what happens when a round passes in battle:\nevery action to occur is assigned a time number (the times of actions you have queued are displayed on the top bar when you don't have anyone selected). The time number is based on the character's appropriate speed stat (melee, ranged, or magic) plus the chosen ability's time modifier (visible in its descriptions when you mouseover it). higher numbers are sooner. in the case that two actions are to occur at equal times, the tie will be broken randomly.\nheals and other things you cast on people that wouldn't want to dodge them will automatically hit. as you already know, the timebar test is given for actions whose targets do want to dodge them. as you do not already know, the pixels per second (pps) at which the timebar fills is randomized for each button from boundaries that are themselves randomized before the attack. the lowerbound can be from 400 to 1500, while the upperbound is from 1500 to 2600. this double randomization allows for some attacks to just be lucky or unlucky and be almost guaranteed to hit or miss, while still giving significant variance between different buttons in the same attack. both boundaries are then multiplied by the ratio of the enemy's accuracy/evasion divided by your character's (so it's fast if the enemy is agile, slow if you are). the ability also has an accuracy multiplier which will factor into the attacker's accuracy.\nthe pps bounds can be changed while you're hitting the buttons, too. for each button you hit, the timebar starts moving 8 percent faster. for each button you miss, it moves 8 percent slower. this is so that even if the enemy is super-evasive you can still hit a few buttons, and vice versa. one more thing about accuracy: in real life it is harder to dodge if you're surrounded by ten adversaries all attacking you at once then if they can only attack you one at a time. to account for this, the target's evasion is divided by 1.25 to the power of the number of attacks they have already dodged that round (otherwise it would be easy to completely avoid damage as long as your evasion is higher than the enemy's accuracy). also for the sake of realisticity, no character can be attacked by more than four adversaries in one round (there's a limit to how much you can surround someone in real life).\nif the ability hit, magnitude is calculated in the same way as time: the acter's strength (or applicable magic affinity) times the ability's magnitude multiplier (3/4 for a counterattack), plus some random variation (+/-15% for most abilities), minus the target's defence (only for attacks, of course). defence is supplemented by any allies that might be in front of the character taking damage (this is where your formation comes into effect). this contribution is equal to 1/20th of allied health (not maxhealth) one row in front, plus 1/15th of allied health two rows forward, plus 1/10th of allied health three rows forward.\n\n\n\n\npress enter to return to the help menu.",(255,255,255),1000)
          pygame.display.flip()
          wait([pygame.K_RETURN],clock)
          window.fill((0,0,0))
          write(window,[455,46],'tutorial menu',(255,255,255))
        if battles.test(event.pos) and battles.down:
          window.fill((0,0,0))
          write(window,[1,1],"help on battles coming later, probably some time after the battle function itself is working or at least under construction. press enter to return to the help menu.",(255,255,255),1000)
          pygame.display.flip()
          wait([pygame.K_RETURN],clock)
          window.fill((0,0,0))
          write(window,[455,46],'tutorial menu',(255,255,255))
        if back.test(event.pos) and back.down:done=True
        controls.down=False;stats.down=False;battles.down=False;back.down=False;pygame.display.flip()
  window.fill((0,0,0))


def new_game(slot,music):
  window.fill((0,0,0))
  write(window,(455,46),'select a difficulty',(255,255,255))
  blue=Button(window,(200,200),(100,20),'blue',(125,125,255),(0,0,255))
  red=Button(window,(450,200),(100,20),'red',(255,0,0),(150,0,0))
  white=Button(window,(700,200),(100,20),'white',(255,255,255),(170,170,170))
  green=Button(window,(200,400),(100,20),'green',(0,255,0),(0,150,0))
  yellow=Button(window,(450,400),(100,20),'Yellow',(255,255,0),(255,150,0))
  black=Button(window,(700,400),(100,20),'black',(85,85,85),(0,0,0))
  description={blue:"the time bar will be a gigantic 860 pixels long.",red:"the time bar will be a generous 720 pixels long.",white:"the time bar will be a reasonable 600 pixels long.",green:"the time bar will be a challenging 500 pixels long.",yellow:"the time bar will be a mere 420 pixels long.",black:"the time bar will be a blatantly unfair 360 pixels long."}
  displayed=None
  pygame.display.flip()
  while True:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEMOTION:
        found=False
        for button in (blue,red,white,green,yellow,black):
          if button.test(event.pos):
            found=True
            if displayed!=button:
              window.fill((0,0,0),(200,300,400,7))
              displayed=button
              write(window,(200,300),description[button],(255,255,255))
              pygame.display.flip()
            break
        if not found and displayed:
          pygame.draw.rect(window,(0,0,0),(200,300,400,7),0)
          displayed=None
          pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONDOWN and event.button==1:
        for button in (blue,red,white,green,yellow,black):
          if button.test(event.pos):button.down=True;pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP and event.button==1:
        for button in (blue,red,white,green,yellow,black):
          if button.test(event.pos) and button.down:
            window.fill((0,0,0))
            write(window,(100,100),"this game takes place in a medieval time period called the black wars, a magical war which involes six factions, each one associated with a color. here are the factions:\n\nthe white guards: simple-minded straightforward guardians of peace and justice. these have a very plain, soldiery attitude. they wield big weapons, heavy armor, unimpressive magic, and an intense hatred for the black faction.\n\nthe red slayers are brutal murderers who kill for no apparent reason. no outsider knows what their real objective is. they are not very powerful, but they are massively numerous. they share the white guard focus on brute force, and have the weakest magic. their nemesis is the blue faction.\n\nthe green serpents: amoralistic and lawless thieves, mercenaries, and assassins. the green serpents are ninjas rather than juggernauts. they carry non-cumbersome weapons, such as daggers and crossbows. they also place more emphasis on magic than the white guards and red slayers. they are less numerous, but far more powerful individually. their nemesis is the Yellow Faction.\n\nthe blue mediators: foolish near-pacifists who rarely actually kill an 'enemy', and never attack an enemy base. the blue mediators are not so much a participant in the war as an anti-blackwar organization. nevertheless, they are often present at battles in an attempt to either mediate a truce or forcefully prevent the battle with their rather strong defensive magic.\n\nthe Yellow Vigilantes: an Idealistic Religious Order, intensely Devoted to a Rigid Idea of Justice. like the green serpents, They prefer speed to brute force. They are the second most powerful, but also the least numerous, due to Their Doctrinal Strictness.\n\nfinally, the black knights: dark, fearsome ninjas who kill with even less mercy than the red faction. their name fills the commonfolk with dread. nobody wants to fight the black knights.\n\npress enter to proceed.",(255,255,255),900)
            pygame.display.flip()
            wait([pygame.K_RETURN],clock)
            music.stop()
            interchapter('preludes')
            global party,chapter
            save=File(gamedir+'saves/'+slot)
            save.set('chapter','preludes')
            save.set('event','visitation')
            save.set('saved stats',{})
            save.set('difficulty',{blue:'blue',red:'red',white:'white',green:'green',yellow:'yellow',black:'black'}[button])
            save.write()
            save.close()
            window.fill((0,0,0))
            pygame.display.flip()
            load_game(slot,music)
          button.down=False;pygame.display.flip()

def get_slot():
  window.fill((0,0,0))
  write(window,[455,47],'choose a save slot',(255,255,255))
  slot1=Button(window,[200,200],[100,20],'slot 1',(255,255,255),(0,0,0))
  slot2=Button(window,[450,200],[100,20],'slot 2',(255,255,255),(0,0,0))
  slot3=Button(window,[700,200],[100,20],'slot 3',(255,255,255),(0,0,0))
  slot4=Button(window,[200,400],[100,20],'slot 4',(255,255,255),(0,0,0))
  slot5=Button(window,[450,400],[100,20],'slot 5',(255,255,255),(0,0,0))
  slot6=Button(window,[700,400],[100,20],'slot 6',(255,255,255),(0,0,0))
  pygame.display.flip()
  while True:
    clock.tick(20)
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.MOUSEBUTTONDOWN and event.button==1:
        for button in (slot1,slot2,slot3,slot4,slot5,slot6):
          if button.test(event.pos):button.down=True;pygame.display.flip()
      if event.type==pygame.MOUSEBUTTONUP and event.button==1:
        for button in (slot1,slot2,slot3,slot4,slot5,slot6):
          if button.test(event.pos) and button.down:return {slot1:'1',slot2:'2',slot3:'3',slot4:'4',slot5:'5',slot6:'6'}[button]

def load_game(slot,music):
  if music:music.stop()
  save=File(gamedir+'saves/'+slot)
  chapter=save.read('chapter')
  event=save.read('event')
  global difficulty
  difficulty=save.read('difficulty')
  file=File(gamedir+'story/'+chapter+'/'+event)
  type=file.read('type')
  if type=='conversation':
    try:
      music=pygame.mixer.Sound(gamedir+'sound/music/'+file.read('music'))
      music.play(-1)
    except:music=None
    dialogue(chapter+'/'+event,music)
    next=file.read('next')
  elif type=='field':
    location=file.read('location')
    startingstate=file.read('startingstate')
    objective=file.read('objective')
    objstring=file.read('objstring')
    music=file.read('music')
    imagefiles=file.read('images')
    speed=file.read('speed')
    if music:
      music=pygame.mixer.Sound(gamedir+'sound/music/'+music)
      music.play(-1)
    images={}
    for direction in imagefiles:
      images[direction]=gamedir+'images/characters/'+imagefiles[direction]
    field(location,images,speed,startingstate,objective,objstring,music)
    next=file.read('next')
  elif type=='fight':
    player=Team([],[],[],[]);enemies=Team([],[],[],[])
    AI=file.read('AI')
    imports=file.read('import')
    for character in file.read('player'):
      stats=file.read(character)
      character=Character(character,gamedir+'images/characters/'+stats['image'],stats['type'],stats['unit'],[500,400])
      if 'life' in stats:character.life=stats['life']
      if 'mana' in stats:character.mana=stats['mana']
      for name in imports:
        if name==character.name:
          status=save.read('saved stats')[imports[name]]
          character.life=status['life']
          character.mana=status['mana']
          character.overflow_check()
          if 'life' in stats:character.life+=stats['life']
          if 'mana' in stats:character.mana+=stats['mana']
      if 'flee' in character.abilities:del character.abilities[character.abilities.index('flee')]
      if stats['row']==1:player.front.append(character)
      elif stats['row']==2:player.midfront.append(character)
      elif stats['row']==3:player.midback.append(character)
      else:player.back.append(character)
    for character in file.read('enemies'):
      stats=file.read(character)
      character=Character(character,gamedir+'images/characters/'+stats['image'],stats['type'],stats['unit'],[500,400])
      if 'life' in stats:character.life=stats['life']
      if 'mana' in stats:character.mana=stats['mana']
      for name in imports:
        if name==character.name:
          status=save.read(imports[name])
          character.life=status['life']
          character.mana=status['mana']
          character.overflow_check()
          if 'life' in stats:character.life=stats['life']
          if 'mana' in stats:character.mana=stats['mana']
      if stats['row']==1:enemies.front.append(character)
      elif stats['row']==2:enemies.midfront.append(character)
      elif stats['row']==3:enemies.midback.append(character)
      else:enemies.back.append(character)
    music=pygame.mixer.Sound(gamedir+'sound/music/'+file.read('music'))
    fight=Fight(player,enemies,AI,file.read('environment'),mandatory=True)
    music.play(-1)
    result=None
    while not result:
      fight.round()
      result=fight.over()
    music.stop()
    if result=='victory':
      music=pygame.mixer.Sound(gamedir+'sound/music/victory.ogg')
      music.play()
      pygame.time.delay(3000)
      next=file.read('next')
    if result=='defeat':
      music=pygame.mixer.Sound(gamedir+'sound/music/the_traditional_blue_mediator_melody_of_lamentation.ogg')
      music.play()
      pygame.time.delay(8000)
      print('program will now exit');sys.exit()
    exports=file.read('exports')
    saved_stats=save.read('saved stats')
    for name in exports:
      for character in player.units()+enemies.units():
        if name==character.name:export={'life':character.life,'mana':character.mana}
        break
      saved_stats[name]=exports
    save.set('saved stats',saved_stats)
  if type=='battle':pass
  if next[0]!=save.read('chapter'):interchapter(next[0])
  save.set('chapter',next[0])
  save.set('event',next[1])
  save.write()
  save.close()
  load_game(slot,music)

def dialogue(filename,music):
  window.fill((0,0,0),(0,0,1000,700))
  file=File(gamedir+'conversations/'+filename)
  sprites={}
  index=0
  while True:
    for event in pygame.event.get():pass # discard any events that occured during the execution of the last event; this is to prevent lines from being skipped
    window.fill((0,0,0),(0,10,1000,90))
    window.fill((0,0,0),(0,0,1000,10))
    pygame.draw.line(window,(255,255,255),(0,10),(1000,10))
    pygame.draw.line(window,(255,255,255),(0,100),(1000,100))
    try:event=file.read(str(index))
    except:break
    if event.__class__==int:pygame.time.delay(event)
    elif event.__class__==str:
      if event[0]=='N':env=load_area(event[1:],mode='cinematic-nosprites')
      else:env=load_area(event,mode='cinematic')
      sprites={}
    elif event.__class__==dict:
      destinations={}
      for spritename in event:
        if spritename not in sprites:
          sprites[spritename]=Sprite(window,gamedir+'images/'+spritename,event[spritename])
          sprites[spritename].draw()
          pygame.display.flip()
        elif event[spritename]=='delete':
          sprites[spritename].erase(env)
          for other in sprites: # gotta redraw any other sprites it might have been overlapping
            other=sprites[other]
            if other is sprites[spritename]:continue # don't want it counting itself among those
            if other.rect_overlaptest(sprites[spritename]):other.draw()
          del sprites[spritename]
          pygame.display.flip()
        elif len(event[spritename])>2:
          destinations[spritename]=event[spritename][0:3]
          try:destinations[spritename].append(abs((destinations[spritename][0]-sprites[spritename].pos[0])/(destinations[spritename][1]-sprites[spritename].pos[1]))) # it's complicated, I know, but it calculates the ratio of the x distance to the y distance.
          except ZeroDivisionError:destinations[spritename].append(1000) # if the y distance (the denominator) was zero then we should set the original ratio to a very high number to ensure that the code below only ever thinks about the x distance.
          destinations[spritename]+=event[spritename][3:]
      alldone=False
      while not alldone: # move sprites
        alldone=True
        clock.tick(32)
        for spritename in destinations: # for each iteration of this, each sprites moves once
          sprite=sprites[spritename]
          dest=destinations[spritename]
          distance=[dest[0]-sprite.pos[0],dest[1]-sprite.pos[1]]
          if distance==[0,0]:continue
          if alldone:alldone=False
          sprite.erase(env)
          for other in sprites:
            other=sprites[other]
            if other is sprite:continue
            if sprite.rect_overlaptest(other):sprite.erase(other)
          for i in range(dest[2]):
            try:ratio=abs(distance[0]/distance[1])
            except ZeroDivisionError:ratio=1000
            if ratio>=dest[3] and distance[0]:
              if distance[0]>0:
                distance[0]-=1
                sprite.pos[0]+=1
              if distance[0]<0:
                distance[0]+=1
                sprite.pos[0]-=1
            else:
              if distance[1]>0:
                distance[1]-=1
                sprite.pos[1]+=1
              if distance[1]<0:
                distance[1]+=1
                sprite.pos[1]-=1
          sprite.draw()
          for foreground in dest[4:]:
            other=sprites[foreground]
            if sprite.rect_overlaptest(other):sprite.erase(other)
        pygame.display.flip()
    elif event.__class__==list:
      if len(event)==1:
        command=event[0]
        if type(command)==int:
          if command==0:
            for x in range(1000):
              for y in range(100,700):
                pos=(x,y)
                color=window.get_at(pos)
                color=(int(color[0]*0.5),int(color[1]*0.5),int(color[2]*0.5))
                window.set_at(pos,color)
                env.image[pos]=color
            pygame.display.flip()
        elif not command:music.stop()
        elif command[0:3]=='fx/':
          effect=pygame.mixer.Sound(gamedir+'sound/'+command)
          effect.play()
        else:
          music.stop()
          music=pygame.mixer.Sound(gamedir+'sound/music/'+command)
          music.play(-1)
      elif len(event)==2:
        if type(event[1])==list:
          env.image=draw_layer([event[0]]+event[1],env.image)
          pygame.display.flip()
        else:
          speaker=event[0]
          words=event[1]
          write(window,(1,1),speaker,(255,255,255))
          write(window,(1,12),words,(255,255,255),990)
          pygame.display.flip()
          wait([pygame.K_RETURN],clock)
      elif len(event)==3: # this tool is used for declaring areas of the map to be sprites from conversation files.
        image={}
        dimensions=event[2]
        for x in range(dimensions[0],dimensions[0]+dimensions[2]):
          for y in range(dimensions[1],dimensions[1]+dimensions[3]):
            image[x,y]=window.get_at([x,y])
        sprites[event[1]]=Sprite(window,image,(0,0))
    index+=1
  if music:music.stop()

def draw_layer(layer,env=None):
  """called by load_area() and occasionally dialogue()."""
  if layer[0]=='inscription':
    write(window,layer[3:5],layer[1],layer[2],layer[5])
  if layer[0]=='bladeless grass':
    pygame.draw.rect(window,(0,200,0),layer[1:],0)
  if layer[0]=='grass':
    pygame.draw.rect(window,(0,200,0),layer[1:],0)
    xstart=layer[1]
    ystart=layer[2]
    xlen=layer[3]
    ylen=layer[4]
    for x in range(int(xlen/16)):
      for y in range(int(ylen/16)):
        position=(xstart+x*16+random.randint(0,15),ystart+y*16+random.randint(0,15))
        pygame.draw.line(window,(0,255,0),position,(position[0],position[1]-2))
  if layer[0]=='grass hill':
    radius=layer[3]
    pos=layer[1:3]
    sectwidth=int(radius/16+0.5)
    pygame.draw.circle(window,(0,202,0),pos,radius,0)
    pygame.draw.circle(window,(0,204,0),pos,radius-sectwidth,0)
    pygame.draw.circle(window,(0,206,0),pos,radius-2*sectwidth,0)
    pygame.draw.circle(window,(0,208,0),pos,radius-3*sectwidth,0)
    pygame.draw.circle(window,(0,210,0),pos,radius-4*sectwidth,0)
    pygame.draw.circle(window,(0,212,0),pos,radius-5*sectwidth,0)
    pygame.draw.circle(window,(0,214,0),pos,radius-6*sectwidth,0)
    pygame.draw.circle(window,(0,216,0),pos,radius-7*sectwidth,0)
    pygame.draw.circle(window,(0,218,0),pos,radius-8*sectwidth,0)
    pygame.draw.circle(window,(0,220,0),pos,radius-9*sectwidth,0)
    pygame.draw.circle(window,(0,222,0),pos,radius-10*sectwidth,0)
    pygame.draw.circle(window,(0,224,0),pos,radius-11*sectwidth,0)
    pygame.draw.circle(window,(0,226,0),pos,radius-12*sectwidth,0)
    pygame.draw.circle(window,(0,228,0),pos,radius-13*sectwidth,0)
    pygame.draw.circle(window,(0,230,0),pos,radius-14*sectwidth,0)
    pygame.draw.circle(window,(0,232,0),pos,radius-15*sectwidth,0)
    dimensions=[layer[1]-layer[3],layer[2]-layer[3],2*layer[3],2*layer[3]]
    for x in range(int(dimensions[0]/16),int((dimensions[0]+dimensions[2])/16)):
      for y in range(int(dimensions[1]/16),int((dimensions[1]+dimensions[3])/16)):
        position=(x*16+random.randint(0,15),y*16+random.randint(0,15))
        pygame.draw.line(window,(0,255,0),position,(position[0],position[1]-2))
  elif layer[0]=='dirt':
    pygame.draw.rect(window,(255,200,80),layer[1:],0)
  elif layer[0]=='path':
    pygame.draw.rect(window,(170,170,170),layer[1:],0)
  elif layer[0]=='black':
    pygame.draw.rect(window,(0,0,0),layer[1:],0)
  elif layer[0]=='wood wall':
    pygame.draw.rect(window,(60,30,0),layer[1:],0)
  elif layer[0]=='wood wall - vertical':
    pygame.draw.rect(window,(60,30,0),layer[1:],0)
    boardsize=int(layer[3]/8+0.5)
    for x in range(8):pygame.draw.line(window,(30,15,0),(x*boardsize+layer[1],layer[2]),(x*boardsize+layer[1],layer[2]+layer[4]-1))
  elif layer[0]=='wood wall - horizontal':
    pygame.draw.rect(window,[60,30,0],layer[1:],0)
    boardsize=int(layer[4]/8+0.5)
    for y in range(8):pygame.draw.line(window,(30,15,0),(layer[1],y*boardsize+layer[2]),(layer[1]+layer[3]-1,y*boardsize+layer[2]))
  elif layer[0]=='wood wall - bottomleft corner':
    pygame.draw.rect(window,(60,30,0),layer[1:],0)
    pygame.draw.line(window,(30,15,0),(layer[1],layer[2]+layer[4]-1),(layer[1]+layer[3]-1,layer[2]))
    vboardwidth=int(layer[3]/8+0.5)
    hboardwidth=int(layer[4]/8+0.5)
    for x in range(8):pygame.draw.line(window,(30,15,0),(x*vboardwidth+layer[1],layer[2]),(x*vboardwidth+layer[1],layer[2]+(8-x)*hboardwidth))
    for y in range(8):pygame.draw.line(window,(30,15,0),((8-y)*hboardwidth+layer[1],y*hboardwidth+layer[2]),(layer[1]+layer[3],layer[2]+y*vboardwidth))
  elif layer[0]=='wood floor':
    pygame.draw.rect(window,(80,40,0),layer[1:],0)
    for x in range(int(layer[3]/50)+1):pygame.draw.line(window,(0,0,0),[x*50+layer[1],layer[2]],[x*50+layer[1],layer[2]+layer[4]])
  elif layer[0]=='roof':
    pygame.draw.rect(window,(40,20,0),layer[1:],0)
    xstart=layer[1]
    ystart=layer[2]
    xlen=layer[3]
    ylen=layer[4]
    offset=False
    for y in range(int(ylen/8)):
      if offset:
        for x in range(int((xlen-4)/8)):pygame.draw.rect(window,[30,15,0],[4+xstart+x*8,ystart+y*8,8,9],1)
      else:
        for x in range(int(xlen/8)):pygame.draw.rect(window,[30,15,0],[xstart+x*8,ystart+y*8,8,9],1)
      offset=toggle(offset)
  elif layer[0]=='door':
    pygame.draw.rect(window,(0,0,0),layer[1:],1)
    pygame.draw.rect(window,(60,30,0),[layer[1]+1,layer[2]+1,layer[3]-2,layer[4]-2],0)
  elif layer[0]=='etwood wall - east':
    pygame.draw.rect(window,(85,85,85),layer[1:],0)
  elif layer[0]=='etwood gate - east':
    for x in range(int(layer[3]/10)):pygame.draw.line(window,(63,63,63),((x+1)*10+layer[1],layer[2]+1),((x+1)*10+layer[1],layer[2]+layer[4]-2),3)
    for y in range(int(layer[4]/10)+1):pygame.draw.line(window,(63,63,63),(layer[1],y*10+layer[2]),(layer[1]+layer[3],y*10+layer[2]),3)
  if env:
    for x in range(layer[1],layer[1]+layer[3]):
      for y in range(layer[2],layer[2]+layer[4]):
        env[x,y]=window.get_at((x,y))
    return env

def load_area(location,mode='field'):
  window.fill((0,0,0),(0,0,1000,700))
  write(window,(475,46),'loading...',(255,255,255))
  pygame.display.flip()
  index=0
  if mode=='battle':location='backgrounds/'+location
  map=File(gamedir+'maps/'+location)
  while True:
    index+=1
    try:layer=map.read('layer'+str(index))
    except KeyError:break
    draw_layer(layer)
  index=0
  while True:
    if mode=='cinematic-nosprites':index=-1
    index+=1
    try:image=map.read('image'+str(index))
    except:break
    sprite=Sprite(window,gamedir+'images/'+image[0],image[1:])
    sprite.draw()
  if mode=='field':walls=map.read('walls')
  background={}
  for x in range(1000):
    for y in range(100,700):
      background[x,y]=window.get_at([x,y])
  if mode=='field':borderers={'up':map.read('up'),'down':map.read('down'),'left':map.read('left'),'right':map.read('right')}
  map.close()
  window.fill((0,0,0),(475,46,75,7))
  if mode=='field':return borderers,walls,Sprite(window,background,(0,0))
  else:return Sprite(window,background,(0,0))

def field(location,images,speed,state,objective,objstring,music,position=None):
  window.fill((0,0,0),(0,0,1000,700))
  destarea=None
  if type(objective)==str and location==objective:return 'objective completed'
  elif type(objective)==list and location==objective[0]:destarea=objective[1]
  borderers,walls,env=load_area(location,mode='field')
  env.draw()
  direction='up'
  olddir='up'
  character=Sprite(window,images[state[2]],state[0:2])
  character.draw()
  panel(objstring)
  pygame.display.flip()
  index=0
  velocity=None
  while True:
    for event in pygame.event.get():
      if event.type==pygame.QUIT:pygame.quit();sys.exit()
      if event.type==pygame.KEYDOWN:
        if event.key==pygame.K_UP:velocity=(0,-speed);direction='up'
        elif event.key==pygame.K_DOWN:velocity=(0,speed);direction='down'
        elif event.key==pygame.K_LEFT:velocity=(-speed,0);direction='left'
        elif event.key==pygame.K_RIGHT:velocity=(speed,0);direction='right'
      if event.type==pygame.KEYUP:
        if event.key==pygame.K_UP and velocity==(0,-speed) or event.key==pygame.K_DOWN and velocity==(0,speed) or event.key==pygame.K_LEFT and velocity==(-speed,0) or event.key==pygame.K_RIGHT and velocity==(speed,0):velocity=None
    if direction!=olddir:
      olddir=direction[:]
      character.erase(env)
      character.load_image(images[direction])
      character.draw()
      pygame.display.flip()
    clock.tick(32)
    if velocity:
      blocked=False
      if direction=='up':
        for wall in walls:
          if wall[0]=='v':continue
          ydist=character.top()-wall[1]
          if ydist<speed and ydist>=0 and rangeoverlap(wall[2],character.left(),character.right(),wall[3]):blocked=True;break
      elif direction=='down':
        for wall in walls:
          if wall[0]=='v':continue
          ydist=wall[1]-character.bottom()
          if ydist<speed and ydist>=0 and rangeoverlap(wall[2],character.left(),character.right(),wall[3]):blocked=True;break
      elif direction=='left':
        for wall in walls:
          if wall[0]=='h':continue
          xdist=character.left()-wall[1]
          if xdist<speed and xdist>=0 and rangeoverlap(wall[2],character.top(),character.bottom(),wall[3]):blocked=True;break
      elif direction=='right':
        for wall in walls:
          if wall[0]=='h':continue
          xdist=wall[1]-character.right()
          if xdist<speed and xdist>=0 and rangeoverlap(wall[2],character.top(),character.bottom(),wall[3]):blocked=True;break
      if not blocked:
        character.move(velocity[0],velocity[1],background=(env,))
        pygame.display.flip()
        if destarea:
          if character.bottom()>=destarea[1] and character.right()>=destarea[0] and character.left()<=destarea[2] and character.top()<=destarea[3]:return 'objective completed'
      if direction=='up' and character.top()<105:return field(borderers['up'],images,speed,[character.pos[0],595,direction],objective,objstring,music)
      if direction=='down' and character.bottom()>695:return field(borderers['down'],images,speed,[character.pos[0],200,direction],objective,objstring,music)
      if direction=='left' and character.left()<5:return field(borderers['left'],images,speed,[985,character.pos[1],direction],objective,objstring,music)
      if direction=='right' and character.right()>995:return field(borderers['right'],images,speed,[15,character.pos[1],direction],objective,objstring,music)
  try:music.stop()
  except:pass

def panel(obj):
  pygame.draw.rect(window,(0,0,0),(0,0,1000,100),0)
  write(window,[1,1],'objective: '+obj,(255,255,255),1000)
  pygame.display.flip()

def interchapter(chapter): # chapter is the chapter being started, not the one just finished
  window.fill((0,0,0))
  music=pygame.mixer.Sound(gamedir+'sound/music/a_chromatic_adventure.ogg')
  music.play(-1)
  if chapter=='preludes':
    write(window,(444,100),"chapter 1: preludes",(255,255,255))
    write(window,(100,200),"This game will begin with following Arkterus, a Blacksmith's Son destined to become the Hero of the Yellow Faction. As the town of Cantor was just ravaged by red slayers, His previously quite wealthy uncle simon has fled with his wife, millicent, and his two children, serpentus and lily, to Etwood, Arkterus's hometown. Arkterus and his Father, Kajin (his mother is not there) are coming to pay them a surprise welcome visit. (press enter again)",(255,255,255),900)
    pygame.display.flip()
    wait([pygame.K_RETURN],clock)
  if chapter=='developments':
    write(window,(432,100),"chapter 2: developments",(255,255,255))
    write(window,(100,200),"Arkterus, meanwhile, is practicing the telekinesis that Kajin has taught Him. Magic is supposed to be impossible for nonfactional people, you see, but Kajin inexplicably developed it about a month ago with remarkable speed and without receiving any sort of instruction, and He has been teaching Arkterus, who also shouldn't be able to learn it but somehow is. It is very strange. Arkterus once went to the Yellow Vigilante Bureau to ask if They knew of any way such a thing might be possible, but They said they didn't. In fact They seemed suspiciously disinterested in the anomaly - considering it outright violated the \"known\" rules of magic. Kajin has never been very curious about it either, so that Arkterus often wonders if He and/or the Yellow Vigilantes actually do know something about it. He doesn't know it, but he will soon discover the truth about it (although not this chapter)...",(255,255,255),900)
    pygame.display.flip()
    wait([pygame.K_RETURN],clock)
  if chapter=='action':
    write(window,(432,100),"chapter 3: action",(255,255,255))
    write(window,(100,200),"",(255,255,255),900)
    pygame.display.flip()
    wait([pygame.K_RETURN],clock)
  music.stop()

ABILITY_DESCRIPTIONS={'attack':"the basic melee attack.",'stab':"a heavier attack than the basic one. has a damage multiplier of 130%, an accuracy multiplier of 85%, and a time modifier of -1. costs 3 mana.",'speed slash':"a quick but weak strike good for dodgy enemies. has a strength multiplier of 80%, an accuracy multiplier of 125%, and a time modifier of +2. costs 3 mana.",'electric strike':"uses magic to electrify your sword for a powerful but expensive strike. 130% strength, 105% accuracy, and a -1 time modifier. costs 5 mana.",\
                      'guard':'increases your defence by 1/10th of your maxlife for one round. has a time modifier of +100.','defend':'increases your defence by 1/20th of your maxlife and raises your evasion by 25% for one round. has a time modifier of +100.','flee':"not yet installed. the game will probably crash if you try this.",\
#                      'fireball':''
                      NotImplemented:""} # this last one is to prevent a KeyError, see the function get_action above
ABILITY_COSTS={'guard':0,'defend':0,'flee':0,'attack':0,'shoot':0,'stab':3,'speed slash':3,'electric strike':5,'flaming smash':6,'fireball':4,'heal':4}
ABILITY_SPEEDS={'attack':0,'stab':-1,'speed slash':2,'electric strike':-1,'flaming smash':-3,'shoot':0,'guard':100,'defend':100,'flee':100}
ABILITY_TYPES={'attack':'melee','stab':'melee','speed slash':'melee','electric strike':'melee','flaming smash':'melee','shoot':'ranged'}
NON_TARGETED={'guard','defend','flee'}
SINGLE_TARGETED={'attack','speed slash','stab','electric strike','flaming smash','shoot'}
ALLY_TARGETED={}
DEAD_TARGETED={}
MELEE_ABILITIES={'attack','speed slash','stab','electric strike','flaming smash'}
RANGED_ABILITIES={}#{'shoot','headshot','power shot'}
OFF_MAGIC_ABILITIES={}#{'icicle','fireball','light blast','rock','zap','shadow'}
DEF_MAGIC_ABILITIES={}#{'heal','zephyr','shield'}
PSV_MAGIC_ABILITIES={}
MISC_ABILITIES={'guard','defend','flee'}
ACCURATE_ABILITIES={'speed slash','flaming smash'}
NONACCURATE_ABILITIES={'attack','shoot'}
INACCURATE_ABILITIES={'stab'}
DAMAGING_ABILITIES={'stab','electric strike','flaming smash'}
NONDAMAGING_ABILITIES={'attack','shoot'}
WEAK_ABILITIES={'speed slash'}
timebar_lengths={'blue':860,'red':720,'white':600,'green':500,'yellow':420,'black':360}
melee_img=File(gamedir+'images/icons/melee_action').read('image')
ranged_img=File(gamedir+'images/icons/ranged_action').read('image')
off_mgk_img=File(gamedir+'images/icons/off_mgk_action').read('image')
def_mgk_img=File(gamedir+'images/icons/def_mgk_action').read('image')
psv_mgk_img=File(gamedir+'images/icons/psv_mgk_action').read('image')
misc_img=File(gamedir+'images/icons/misc_action').read('image')
window=pygame.display.set_mode((1000,700))
clock=pygame.time.Clock()
if __name__=='__main__':main_menu()
