import pygame
import sys
from pyware import *

def save():
  hide()
  image={}
  x=int(input('x:'))
  y=int(input('y:'))
  point=(x,y)
  for x in range(300):
    for y in range(300):
      pixel=window.get_at((x,y))
      if pixel!=background:image[(x-point[0],y-point[1])]=pixel
  file.set('image',image)
  file.write()

def load(image):
 for pixel in image:window.set_at((pixel[0]+150,pixel[1]+150),image[pixel])

def draw():window.set_at(position,color);pygame.display.flip()
def show():window.set_at(position,pointer);pygame.display.flip()
def hide():window.set_at(position,hidden);pygame.display.flip()
def cycle():colors[0]=colors[1];colors[1]=colors[2];colors[2]=color

if len(sys.argv)>1:file=File(sys.argv[1])
else:file=File(input('filename:'))
window=pygame.display.set_mode((300,300),0,32)
background=(38,38,38,255)
window.fill(background)
try:load(file.read('image'))
except:None
clock=pygame.time.Clock()
pendown=False
colors=[(255,0,0),(0,255,0),(0,0,255),background]
color=(255,255,255,255)
pointer=(128,128,128,255)
position=[150,150]
shiftdown=False
hidden=window.get_at(position)
show()
pygame.display.flip()

while True:
  clock.tick(10)
  for event in pygame.event.get():
    if event.type==pygame.QUIT:sys.exit()
    if event.type==pygame.KEYDOWN:
      if event.key==pygame.K_RETURN:
        try:save()
        except:print('error')
      elif event.key==pygame.K_UP:
        hide()
        if not shiftdown:position[1]-=1
        else:position[1]-=5
        if pendown:draw()
        hidden=window.get_at(position)
        show()
      elif event.key==pygame.K_DOWN:
        hide()
        if not shiftdown:position[1]+=1
        else:position[1]+=5
        if pendown:draw()
        hidden=window.get_at(position)
        show()
      elif event.key==pygame.K_LEFT:
        hide()
        if not shiftdown:position[0]-=1
        else:position[0]-=5
        if pendown:draw()
        hidden=window.get_at(position)
        show()
      elif event.key==pygame.K_RIGHT:
        hide()
        if not shiftdown:position[0]+=1
        else:position[0]+=5
        if pendown:draw()
        hidden=window.get_at(position)
        show()
      elif event.key==pygame.K_SPACE:
        pendown=toggle(pendown)
        print(pendown)
      elif event.key in {pygame.K_LSHIFT,pygame.K_RSHIFT}:shiftdown=True
      elif event.key==pygame.K_d:
        if not shiftdown:draw()
        else:
          try:window.set_at(position,[int(input('r:')),int(input('g:')),int(input('b:'))]);pygame.display.flip()
          except:print('error')
        hidden=window.get_at(position);show()
      elif event.key==pygame.K_s:show()
      elif event.key==pygame.K_h:hide()
      elif event.key==pygame.K_g:
        try:hide();position=[int(input('x:')),int(input('y:'))];show()
        except:print('error')
      elif event.key==pygame.K_c:
        try:color=[int(input('r:')),int(input('g:')),int(input('b:')),255];cycle()
        except:print('error')
      elif event.key==pygame.K_a:hide();cycle();color=window.get_at(position);show();print(color)
      elif event.key==pygame.K_p:
        try:pointer=[int(input('r:')),int(input('g:')),int(input('b:')),255]
        except:print('error')
      elif event.key==pygame.K_w:hide();print(position,window.get_at(position));show()
      elif event.key==pygame.K_m:
        try:
          dimensions=[int(input('startx:')),int(input('starty:')),int(input('xlen:')),int(input('ylen:'))]
          move=[int(input('xmove:')),int(input('ymove:'))]
        except:print('error');continue
        for x in range(dimensions[0],dimensions[0]+dimensions[2]):
          for y in range(dimensions[1],dimensions[1]+dimensions[3]):
            c=window.get_at([x,y])
            if c==background:continue
            window.set_at([x+move[0],y+move[1]],c)
            window.set_at([x,y],background)
      elif event.key==pygame.K_i:
        hide()
        mode=input("specify:")
        if mode=='v':
          for x in range(300):
            for y in range(150):
              pixcolor=window.get_at([x,y])
              revcolor=window.get_at([x,299-y])
              if pixcolor!=background or revcolor!=background:
                window.set_at([x,299-y],pixcolor)
                window.set_at([x,y],revcolor)
        if mode=='h':
          for x in range(150):
            for y in range(300):
              pixcolor=window.get_at([x,y])
              revcolor=window.get_at([299-x,y])
              if pixcolor!=background or revcolor!=background:
                window.set_at([299-x,y],pixcolor)
                window.set_at([x,y],revcolor)
        if mode=='c':
          image={}
          for x in range(300):
            for y in range(300):
              pixel=window.get_at((x,y))
              if pixel!=background:image[(x,y)]=pixel
          window.fill(background)
          for pixel in image:
            window.set_at([300-pixel[1],pixel[0]],image[pixel])
        if mode=='top':
          for x in range(300):
            for y in range(150):window.set_at([x,299-y],window.get_at([x,y]))
        if mode=='bottom':
          for x in range(300):
            for y in range(150,300):window.set_at([x,299-y],window.get_at([x,y]))
        if mode=='left':
          for x in range(150):
            for y in range(300):window.set_at([299-x,y],window.get_at([x,y]))
        if mode=='right':
          for x in range(150,300):
            for y in range(300):window.set_at([299-x,y],window.get_at([x,y]))
        show()
      elif event.key==pygame.K_l:
        print('current color:',str(color),'\npointer color:',str(pointer),'\ncolors 1-3:',str(colors[:3]),'\nposition:',str(position),'\nhidden pixel\'s color:',str(hidden))
      elif event.key==pygame.K_1:cycle();color=colors[0];print(color)
      elif event.key==pygame.K_2:cycle();color=colors[1];print(color)
      elif event.key==pygame.K_3:cycle();color=colors[2];print(color)
      elif event.key==pygame.K_0:color=colors[3];print(color)

    if event.type==pygame.KEYUP:
      if event.key in {pygame.K_LSHIFT,pygame.K_RSHIFT}:shiftdown=False
