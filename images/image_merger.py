import pyware
images=[]
offsets=[]
while True:
  filename=input('file to merge: ')
  if not filename:break
  file=pyware.File(filename)
  image=file.read('image')
  file.close()
  print('offset to use for this image: ')
  offset=[int(input(' x: ')),int(input(' y: '))]
  images+=[image]
  offsets+=[offset]

components=[]
index=0
for image in images:
  new_image={}
  offset=offsets[index]
  for pixel in image:new_image[(pixel[0]+offset[0],pixel[1]+offset[1])]=image[pixel]
  components+=[new_image]
  index+=1

product={}
for image in components:
  for pixel in image:
    product[pixel]=image[pixel]
    
file=pyware.File(input('file to export new image to: '))
file.set('image',product) 
file.write()
file.close()
