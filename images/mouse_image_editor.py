import pygame,sys,math
from pyware import *

def save():
  image={}
  answer=input('enter save center with text(Y/n)?')
  if answer=='n':
    point=None
    while not point:
      for event in pygame.event.get():
        if event.type==pygame.MOUSEBUTTONDOWN:point=event.pos
  else:
    x=int(input('x:'))
    y=int(input('y:'))
    point=(x,y)
  #old=input('old format(y/N)? ')
  old='y'
  if old!='y':
    for x in range(300):
      for y in range(300):
        color=window.get_at((x,y))
        color=(color[0],color[1],color[2])
        if color in image:image[color]+=[[x,y]]
        else:image[color]=[[x,y]]
  else:
    for x in range(300):
      for y in range(300):
        pixel=window.get_at((x,y))
        if pixel!=(8,8,8,255):image[(x-point[0],y-point[1])]=pixel
  file.set('image',image)
  file.write()

def load(image):
 for pixel in image:window.set_at((pixel[0]+150,pixel[1]+150),image[pixel])

def text():
 global color
 mode=input('select action (coords, line, point, circle, color, import):')
 if mode=='coords':
  point=None
  while not point:
   for event in pygame.event.get():
    if event.type==pygame.MOUSEBUTTONDOWN:point=event.pos
  print(point)
 if mode=='line':pygame.draw.line(window,color,[int(input('start x:')),int(input('start y:'))],[int(input('end x:')),int(input('end y:'))],int(input('thickness:')))
 if mode=='point':pygame.draw.rect(window,color,[int(input('x:')),int(input('y:')),int(input('x-size:')),int(input('y-size:'))],0)
 if mode=='circle':pygame.draw.circle(window,color,[int(input('x:')),int(input('y:'))],int(input('radius:')),int(input('thickness:')))
 if mode=='color':color=[int(input('r:')),int(input('g:')),int(input('b:')),255]
 if mode=='import':
   imagefile=File(input('imagefilepath:'))

file=File(input('filename:'))
window=pygame.display.set_mode((400,300),0,32)
window.fill((8,8,8))
pygame.draw.rect(window,(0,0,255),(300,0,100,300),0)
try:load(file.read('image'))
except:None
mousedown=False;color=(255,255,255,255);size=1;mode='draw'
draw=Button(window,[305,5],[40,10],'draw',(255,255,255),(0,0,0),(0,0,255))
line=Button(window,[355,5],[40,10],'line',(255,255,255),(0,0,0),(0,0,255))
circle=Button(window,[305,20],[40,10],'circle',(255,255,255),(0,0,0),(0,0,255))
rect=Button(window,[355,20],[40,10],'rect',(255,255,255),(0,0,0),(0,0,255))
draw.down=True
draw.draw()
circle.draw()
rect.draw()
pygame.display.flip()

while True:
 
 for event in pygame.event.get():
  if event.type==pygame.QUIT:sys.exit()
  if event.type==pygame.KEYDOWN:
   if event.key==pygame.K_RETURN:save()
   elif event.key==pygame.K_1:color=(255,0,0,255)
   elif event.key==pygame.K_2:color=(0,255,0,255)
   elif event.key==pygame.K_3:color=(0,0,255,255)
   elif event.key==pygame.K_4:color=(255,255,0,255)
   elif event.key==pygame.K_5:color=(255,0,255,255)
   elif event.key==pygame.K_6:color=(0,255,255,255)
   elif event.key==pygame.K_7:color=(255,255,255,255)
   elif event.key==pygame.K_8:color=(127,127,127,255)
   elif event.key==pygame.K_9:color=(0,0,0,255)
   elif event.key==pygame.K_0:color=(8,8,8,255)
   elif event.key==pygame.K_q:size=1
   elif event.key==pygame.K_w:size=2
   elif event.key==pygame.K_e:size=3
   elif event.key==pygame.K_r:size=4
   elif event.key==pygame.K_t:size=5
   elif event.key==pygame.K_y:size=7
   elif event.key==pygame.K_u:size=10
   elif event.key==pygame.K_i:size=15
   elif event.key==pygame.K_o:size=20
   elif event.key==pygame.K_p:size=25
   elif event.key==pygame.K_SPACE:
     try:text()
     except:print('error')
  if event.type==pygame.MOUSEBUTTONDOWN:
   mousedown=True
   if event.pos[0]<300:
    if mode=='draw':
     if size==1:window.set_at(event.pos,color)
     else:pygame.draw.rect(window,color,(event.pos[0]-int(size/2),event.pos[1]-int(size/2),size,size),0)
    if mode=='line':
     start=event.pos
     window.set_at(start,color)
     pygame.display.flip()
     end=None
     while not end:
      for event in pygame.event.get():
       if event.type==pygame.MOUSEBUTTONDOWN:
        end=event.pos
     pygame.draw.line(window,color,start,end,int(input('thickness: ')))
    if mode=='circle':
     ringpoint=None
     center=event.pos
     bgcolor=window.get_at(center)
     window.set_at(center,color)
     pygame.display.flip()
     while not ringpoint:
      for event in pygame.event.get():
       if event.type==pygame.QUIT:sys.exit()
       if event.type==pygame.MOUSEMOTION:
        opposite=[center[0]-(event.pos[0]-center[0]),center[1]-(event.pos[1]-center[1])]
        try:opposite_color=window.get_at(opposite)
        except:opposite_color=(8,8,8,255)
       if event.type==pygame.MOUSEBUTTONDOWN:
        ringpoint=event.pos
        window.set_at(center,bgcolor)
        xdiff=abs(center[0]-ringpoint[0])
        ydiff=abs(center[1]-ringpoint[1])
        dist=math.pow(xdiff,2)+math.pow(ydiff,2)
        dist=math.pow(dist,0.5)
        pygame.draw.circle(window,color,center,int(dist),int(input('thickness: ')))
        pygame.display.flip()
    if mode=='rect':
     vertex1=event.pos
     window.set_at(vertex1,color)
     pygame.display.flip()
     vertex4=None
     while not vertex4:
      for event in pygame.event.get():
       if event.type==pygame.MOUSEBUTTONDOWN:
        vertex4=event.pos
     pygame.draw.rect(window,color,(vertex1[0],vertex1[1],vertex4[0]-vertex1[0],vertex4[1]-vertex1[1]),int(input('thickness: ')))
   else:
    if draw.test(event.pos):
     draw.down=True
     draw.draw()
     mode='draw'
     line.down=False
     circle.down=False
     rect.down=False
    if line.test(event.pos):
     line.down=True
     line.draw()
     mode='line'
     draw.down=False
     circle.down=False
     rect.down=False
    elif circle.test(event.pos):
     circle.down=True
     circle.draw()
     mode='circle'
     draw.down=False
     line.down=False
     rect.down=False
    elif rect.test(event.pos):
     rect.down=True
     rect.draw()
     mode='rect'
     draw.down=False
     line.down=False
     circle.down=False
    pygame.display.flip()

  if event.type==pygame.MOUSEBUTTONUP:
   mousedown=False

  if event.type==pygame.MOUSEMOTION:
   if mousedown and event.pos[0]<300:
    if mode=='draw':
     if size==1:window.set_at(event.pos,color)
     else:pygame.draw.rect(window,color,(event.pos[0]-int(size/2),event.pos[1]-int(size/2),size,size),0)
   pygame.display.flip()
